describe("Testing App", () => {
    it("Search Travel", () => {  
        cy.wait(2000);
        cy.visit({url:"https://tickethackfront.vercel.app/index.html", log:true});
        cy.get('#departure').type('Paris');
        cy.get('#arrival').type('Bruxelles');
        cy.get('#search').click();
        cy.contains('Paris > Bruxelles');
    });  
});